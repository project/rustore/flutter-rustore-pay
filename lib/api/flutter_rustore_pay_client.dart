import 'package:flutter/services.dart';
import 'package:flutter_rustore_pay/model/preferred_purchase_type.dart';

import 'package:flutter_rustore_pay/pigeons/flutter_rustore_pay_api.dart';

import '../model/product.dart';
import '../model/product_type.dart';
import '../model/purchase.dart';
import '../model/purchase_availability.dart';
import '../model/purchase_result.dart';
import '../model/ru_store_exception.dart';

/// Класс для работы с pay sdk, содержит:
/// purchaseInteractor - класс, который позволяет работать с платежами.
/// productInteractor - класс, который позволяет работать с продуктами.
/// ruStoreUtils - класс содержит набор утилитных методов.
class RuStorePayClient {
  RuStorePayClient._privateConstructor();

  static final RuStorePayClient _instance =
      RuStorePayClient._privateConstructor();

  static RuStorePayClient get instance => _instance;

  final RuStoreFlutterUtils ruStoreUtils = _RuStoreFlutterUtilsImpl.instance;

  final RuStoreFlutterProductInteractor productInteractor =
      _RuStoreFlutterProductInteractorImpl.instance;

  final RuStoreFlutterPurchaseInteractor purchaseInteractor =
      _RuStoreFlutterPurchaseInteractorImpl.instance;
}

abstract class RuStoreFlutterPurchaseInteractor {
  /// Позволяет получить информацию о покупке по её ID. Возвращает покупку по её ID.
  Future<Purchase> getPurchase(String purchaseId);

  /// Позволяет получить покупки пользователя. Данный метод поддерживает опциональную фильтрацию по типу товаров (потребляемые или непотреблямые товары). По умолчанию фильтр выключен и вернет всё покупки пользователя (в не зависимости от типа товара) в статусах PAID и CONFIRMED. Возвращает список покупок.
  Future<List<Purchase>> getPurchases({ProductType? productType});

  /// Позволяет получить результат доступности работы с платежами. Возвращает PurchaseAvailabilityResult.
  Future<PurchaseAvailabilityResult> getPurchaseAvailability();

  /// Запускает флоу покупки товара с выбором стадийности оплаты. Возвращает ProductPurchaseResult.
  Future<ProductPurchaseResult> purchase(String productId,
      {String? orderId,
      int? quantity,
      String? developerPayload,
      String? appUserId,
      PreferredPurchaseType? preferredPurchaseType});

  /// Запускает флоу двустадийной покупки товара. Возвращает ProductPurchaseResult.
  Future<ProductPurchaseResult> purchaseTwoStep(String productId,
      {String? orderId, int? quantity, String? developerPayload});

  /// Подтверждение покупки (для покупок, совершенных по двустадийной оплате). В этом случае происходит перевод холдированных средств.
  Future<void> confirmTwoStepPurchase(String purchaseId,
      {String? developerPayload});

  /// Отмена покупки (для покупок, совершенных по двустадийной оплате). В этом случае происходит отмена холдирования средств
  Future<void> cancelTwoStepPurchase(String purchaseId);
}

final class _RuStoreFlutterPurchaseInteractorImpl
    extends RuStoreFlutterPurchaseInteractor {
  _RuStoreFlutterPurchaseInteractorImpl._();

  static _RuStoreFlutterPurchaseInteractorImpl? _instance;

  static RuStoreFlutterPurchaseInteractor get instance =>
      _instance ??= _RuStoreFlutterPurchaseInteractorImpl._();

  final RustorePurchaseApi _api = RustorePurchaseApi();

  @override
  Future<Purchase> getPurchase(String purchaseId) async {
    try {
      final flutterPurchase = await _api.getPurchase(purchaseId);
      return mapFlutterPurchaseToPurchase(flutterPurchase);
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<List<Purchase>> getPurchases({ProductType? productType}) async {
    try {
      final flutterPurchases = await _api.getPurchases(
          productType: _mapProductTypeToFlutterProductType(productType));
      return flutterPurchases.map(mapFlutterPurchaseToPurchase).toList();
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<PurchaseAvailabilityResult> getPurchaseAvailability() async {
    try {
      final response = await _api.getPurchaseAvailability();
      if (response.availability == true) {
        return Available();
      } else {
        return Unavailable(errorMessage: response.cause);
      }
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<ProductPurchaseResult> purchase(
    String productId, {
    String? orderId,
    int? quantity,
    String? developerPayload,
    String? appUserId,
   PreferredPurchaseType? preferredPurchaseType,
  }) async {
    try {
      final purchaseResult = await _api.purchase(
        productId,
        orderId: orderId,
        quantity: quantity,
        developerPayload: developerPayload,
        appUserId: appUserId,
        preferredPurchaseType: mapFlutterPurchaseTypeToPreferredPurchaseType(
            preferredPurchaseType),
      );
      return mapFlutterPurchaseResultToPurchaseResult(purchaseResult);
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<ProductPurchaseResult> purchaseTwoStep(
    String productId, {
    String? orderId,
    int? quantity,
    String? developerPayload,
  }) async {
    try {
      final purchaseResult = await _api.purchaseTwoStep(productId,
          orderId: orderId,
          quantity: quantity,
          developerPayload: developerPayload);
      return mapFlutterPurchaseResultToPurchaseResult(purchaseResult);
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<void> confirmTwoStepPurchase(String purchaseId,
      {String? developerPayload}) async {
    try {
      return await _api.confirmTwoStepPurchase(purchaseId,
          developerPayload: developerPayload);
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }

  @override
  Future<void> cancelTwoStepPurchase(String purchaseId) async {
    try {
      return await _api.cancelTwoStepPurchase(purchaseId);
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }
}

abstract class RuStoreFlutterProductInteractor {
  /// Позволяет получить список продуктов по их ID (Данный метод имеет ограничение в 1000 продуктов). Возвращает список продуктов.
  Future<List<Product>> getProducts(List<String> ids);
}

final class _RuStoreFlutterProductInteractorImpl
    extends RuStoreFlutterProductInteractor {
  _RuStoreFlutterProductInteractorImpl._();

  static _RuStoreFlutterProductInteractorImpl? _instance;

  static RuStoreFlutterProductInteractor get instance =>
      _instance ??= _RuStoreFlutterProductInteractorImpl._();

  final RustoreProductApi _api = RustoreProductApi();

  @override
  Future<List<Product>> getProducts(List<String> ids) async {
    try {
      final flutterProducts = await _api.getProducts(ids);
      return flutterProducts.map(mapFlutterProductToProduct).toList();
    } on PlatformException catch (error, stackTrace) {
      _handleRuStoreException(error, stackTrace);
      rethrow;
    }
  }
}

abstract class RuStoreFlutterUtils {
  /// Запускает МП RuStore.
  Future<void> openRuStore();

  /// Проверяет наличие приложения RuStore на девайсе пользователя. Возвращает логическое значение.
  Future<bool> isRuStoreInstalled();

  /// Запускает МП RuStore для авторизации. После успешной авторизации пользователя МП RuStore автоматически закроется.
  Future<void> openRuStoreAuthorization();

  /// Открывает web старницу для скачивания МП RuStore.
  Future<void> openRuStoreDownloadInstruction();
}

final class _RuStoreFlutterUtilsImpl extends RuStoreFlutterUtils {
  _RuStoreFlutterUtilsImpl._();

  static _RuStoreFlutterUtilsImpl? _instance;

  static RuStoreFlutterUtils get instance =>
      _instance ??= _RuStoreFlutterUtilsImpl._();

  final RustoreUtilsApi _api = RustoreUtilsApi();

  @override
  Future<void> openRuStore() => _api.openRuStore();

  @override
  Future<bool> isRuStoreInstalled() => _api.isRuStoreInstalled();

  @override
  Future<void> openRuStoreAuthorization() => _api.openRuStoreAuthorization();

  @override
  Future<void> openRuStoreDownloadInstruction() =>
      _api.openRuStoreDownloadInstruction();
}

extension on PlatformException {
  RuStoreException? _makeRuStoreException() {
    // Извлечение сообщения после ': ' если таковое присутствует
    final message = this.message?.split(': ').skip(1).join(': ').trim();

    return switch (code) {
      'RustorePaymentNetworkException' =>
        RustorePaymentNetworkException(message),
      'RustorePaymentCommonException' => RustorePaymentCommonException(message),
      'RuStorePayClientAlreadyExist' => RuStorePayClientAlreadyExist(message),
      'RuStorePayClientNotCreated' => RuStorePayClientNotCreated(message),
      'RuStorePayInvalidActivePurchase' =>
        RuStorePayInvalidActivePurchase(message),
      'RuStorePayInvalidConsoleAppId' => RuStorePayInvalidConsoleAppId(message),
      'RuStorePaySignatureException' => RuStorePaySignatureException(message),
      'EmptyPaymentTokenException' => EmptyPaymentTokenException(message),
      'RuStoreNotInstalledException' => RuStoreNotInstalledException(message),
      'RuStoreOutdatedException' => RuStoreOutdatedException(message),
      'RuStoreUserUnauthorizedException' =>
        RuStoreUserUnauthorizedException(message),
      'RuStoreApplicationBannedException' =>
        RuStoreApplicationBannedException(message),
      'RuStoreUserBannedException' => RuStoreUserBannedException(message),
      _ => null,
    };
  }
}

void _handleRuStoreException(PlatformException error, StackTrace stackTrace) {
  if (error._makeRuStoreException() case RuStoreException exception) {
    Error.throwWithStackTrace(exception, stackTrace);
  }
}

FlutterProductType? _mapProductTypeToFlutterProductType(
    ProductType? productType) {
  return switch (productType) {
    ProductType.consumable => FlutterProductType.consumableProduct,
    ProductType.nonConsumable => FlutterProductType.nonConsumableProduct,
    ProductType.undefined => FlutterProductType.undefined,
    null => null,
  };
}

FlutterPreferredPurchaseType? mapFlutterPurchaseTypeToPreferredPurchaseType(
    PreferredPurchaseType? flutterPurchaseType) {
  return switch (flutterPurchaseType) {
    PreferredPurchaseType.oneStep => FlutterPreferredPurchaseType.oneStep,
    PreferredPurchaseType.twoStep => FlutterPreferredPurchaseType.twoStep,
    null => null
  };
}


