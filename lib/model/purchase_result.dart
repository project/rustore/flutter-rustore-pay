import 'package:flutter_rustore_pay/model/purchase_type.dart';
import 'package:flutter_rustore_pay/pigeons/flutter_rustore_pay_api.dart';

/// Результат покупки.
sealed class ProductPurchaseResult {}

/// Результат успешного завершения покупки цифрового товара. Имеет переопределенные методы equals(), hashcode(), toString().
final class SuccessProductPurchaseResult extends ProductPurchaseResult {
  final String? orderId;
  final String purchaseId;
  final String productId;
  final String invoiceId;
  final PurchaseType purchaseType;
  final bool sandbox;

  SuccessProductPurchaseResult._({
    this.orderId,
    required this.purchaseId,
    required this.productId,
    required this.invoiceId,
    required this.purchaseType,
    required this.sandbox,
  });

  @override
  bool operator ==(covariant SuccessProductPurchaseResult other) {
    if (identical(this, other)) return true;
    return other.orderId == orderId &&
        other.purchaseId == purchaseId &&
        other.productId == productId &&
        other.invoiceId == invoiceId &&
        other.purchaseType == purchaseType &&
        other.sandbox == sandbox;
  }

  @override
  int get hashCode {
    return orderId.hashCode ^
        purchaseId.hashCode ^
        productId.hashCode ^
        invoiceId.hashCode ^
        purchaseType.hashCode ^
        sandbox.hashCode;
  }

  @override
  String toString() =>
      '$SuccessProductPurchaseResult(orderId: $orderId, purchaseId: $purchaseId, productId: $productId, invoiceId: $invoiceId, purchaseType: $purchaseType, sandbox: $sandbox)';
}

/// Результат закрытия платежного диалога до получения результата покупки. Состояние покупки неизвестно. Имеет переопределенные методы equals(), hashcode(), toString().
final class CancelProductPurchaseResult extends ProductPurchaseResult {
  final String? purchaseId;
  final PurchaseType purchaseType;

  CancelProductPurchaseResult._({this.purchaseId, required this.purchaseType});

  @override
  bool operator ==(covariant CancelProductPurchaseResult other) {
    if (identical(this, other)) return true;
    return other.purchaseId == purchaseId && other.purchaseType == purchaseType;
  }

  @override
  int get hashCode => purchaseId.hashCode ^ purchaseType.hashCode;

  @override
  String toString() =>
      '$CancelProductPurchaseResult(purchaseId: $purchaseId, purchaseType: $purchaseType)';
}

/// Результат ошибки покупки цифрового товара. Имеет переопределенные методы equals(), hashcode(), toString().
final class FailureProductPurchaseResult extends ProductPurchaseResult {
  final String? orderId;
  final String? purchaseId;
  final String? productId;
  final String? invoiceId;
  final int? quantity;
  final PurchaseType purchaseType;
  final String errorMessage;

  FailureProductPurchaseResult._({
    this.orderId,
    this.purchaseId,
    this.productId,
    this.invoiceId,
    this.quantity,
    required this.purchaseType,
    required this.errorMessage,
  });

  @override
  bool operator ==(covariant FailureProductPurchaseResult other) {
    if (identical(this, other)) return true;
    return other.orderId == orderId &&
        other.purchaseId == purchaseId &&
        other.productId == productId &&
        other.invoiceId == invoiceId &&
        other.quantity == quantity &&
        other.purchaseType == purchaseType &&
        other.errorMessage == other.errorMessage;
  }

  @override
  int get hashCode {
    return orderId.hashCode ^
        purchaseId.hashCode ^
        productId.hashCode ^
        invoiceId.hashCode ^
        quantity.hashCode ^
        purchaseType.hashCode ^
        errorMessage.hashCode;
  }

  @override
  String toString() =>
      '$FailureProductPurchaseResult(orderId: $orderId, purchaseId: $purchaseId, productId: $productId, invoiceId: $invoiceId, quantity: $quantity, purchaseType: $purchaseType, errorMessage: $errorMessage)';
}

/// Результат покупки цифрового товара не известен. Имеет переопределенные методы equals(), hashcode(), toString().
final class UnknownProductPurchaseResult extends ProductPurchaseResult {
  final String cause;

  UnknownProductPurchaseResult({required this.cause});

  @override
  bool operator ==(covariant UnknownProductPurchaseResult other) {
    if (identical(this, other)) return true;
    return other.cause == cause;
  }

  @override
  int get hashCode => cause.hashCode;

  @override
  String toString() => '$UnknownProductPurchaseResult(cause: $cause)';
}

ProductPurchaseResult mapFlutterPurchaseResultToPurchaseResult(
    FlutterProductPurchaseResult purchaseResult) {
  if (purchaseResult.cancelled case Cancelled cancelled) {
    return CancelProductPurchaseResult._(
        purchaseId: cancelled.purchaseId,
        purchaseType:
            mapFlutterPurchaseTypeToPurchaseType(cancelled.purchaseType));
  } else if (purchaseResult.failure case Failure failure) {
    return FailureProductPurchaseResult._(
        orderId: failure.orderId,
        purchaseId: failure.purchaseId,
        productId: failure.productId,
        invoiceId: failure.invoiceId,
        quantity: failure.quantity,
        purchaseType:
            mapFlutterPurchaseTypeToPurchaseType(failure.purchaseType),
        errorMessage: failure.errorMessage);
  } else if (purchaseResult.success case Success success) {
    return SuccessProductPurchaseResult._(
      orderId: success.orderId,
      purchaseId: success.purchaseId,
      productId: success.productId,
      invoiceId: success.invoiceId,
      purchaseType: mapFlutterPurchaseTypeToPurchaseType(success.purchaseType),
      sandbox: success.sandbox,
    );
  } else {
    return UnknownProductPurchaseResult(cause: 'Unknown purchase result');
  }
}
