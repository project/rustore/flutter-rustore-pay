// Тип покупки для метода purchase (одностадийная / двустадийная).
enum PreferredPurchaseType {
  oneStep,
  twoStep,
}