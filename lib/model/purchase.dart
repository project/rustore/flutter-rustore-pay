import 'package:flutter_rustore_pay/model/product_type.dart';
import 'package:flutter_rustore_pay/model/purchase_type.dart';
import 'package:flutter_rustore_pay/pigeons/flutter_rustore_pay_api.dart';

/// Модель покупки. Имеет переопределенные методы equals(), hashcode(), toString().
final class Purchase {
  final String purchaseId;
  final String productId;
  final String invoiceId;
  final String? orderId;
  final PurchaseType purchaseType;
  final ProductType productType;
  final String description;
  final String? purchaseTime;
  final int price;
  final String amountLabel;
  final String currency;
  final int quantity;
  final PurchaseStatus status;
  final String? developerPayload;
  final bool sandbox;

  Purchase._({
    required this.purchaseId,
    required this.productId,
    required this.invoiceId,
    this.orderId,
    required this.purchaseType,
    required this.productType,
    required this.description,
    this.purchaseTime,
    required this.price,
    required this.amountLabel,
    required this.currency,
    required this.quantity,
    required this.status,
    this.developerPayload,
    required this.sandbox,
  });

  @override
  bool operator ==(covariant Purchase other) {
    if (identical(this, other)) return true;
    return other.purchaseId == purchaseId &&
        other.productId == productId &&
        other.invoiceId == invoiceId &&
        other.orderId == orderId &&
        other.purchaseType == purchaseType &&
        other.productType == productType &&
        other.description == description &&
        other.purchaseTime == purchaseTime &&
        other.price == price &&
        other.amountLabel == amountLabel &&
        other.currency == currency &&
        other.quantity == quantity &&
        other.status == status &&
        other.developerPayload == developerPayload &&
        other.sandbox == sandbox;
  }

  @override
  int get hashCode {
    return purchaseId.hashCode ^
        productId.hashCode ^
        invoiceId.hashCode ^
        orderId.hashCode ^
        purchaseType.hashCode ^
        productType.hashCode ^
        description.hashCode ^
        purchaseTime.hashCode ^
        price.hashCode ^
        amountLabel.hashCode ^
        currency.hashCode ^
        quantity.hashCode ^
        status.hashCode ^
        developerPayload.hashCode ^
        sandbox.hashCode;
  }

  @override
  String toString() =>
      '$Purchase(purchaseId: $purchaseId, productId: $productId, invoiceId: $invoiceId, orderId: $orderId, purchaseType: $purchaseType, productType: $productType, description: $description, purchaseTime: $purchaseTime, price: $price, amountLabel: $amountLabel, currency: $currency, quantity: $quantity, status: $status, developerPayload: $developerPayload, sandbox: $sandbox)';
}

/// Возможные значения состояния покупки.
enum PurchaseStatus {
  invoiceCreated,
  cancelled,
  processing,
  rejected,
  confirmed,
  refunded,
  paid,
  reversed,
  unknown;
}

Purchase mapFlutterPurchaseToPurchase(FlutterPurchase flutterPurchase) {
  return Purchase._(
    purchaseId: flutterPurchase.purchaseId,
    productId: flutterPurchase.productId,
    invoiceId: flutterPurchase.invoiceId,
    orderId: flutterPurchase.orderId,
    purchaseType:
        mapFlutterPurchaseTypeToPurchaseType(flutterPurchase.purchaseType),
    productType:
        mapFlutterProductTypeToProductType(flutterPurchase.productType),
    description: flutterPurchase.description,
    purchaseTime: flutterPurchase.purchaseTime,
    price: flutterPurchase.price,
    amountLabel: flutterPurchase.amountLabel,
    currency: flutterPurchase.currency,
    quantity: flutterPurchase.quantity,
    status: mapFlutterPurchaseStatusToPurchaseStatus(flutterPurchase.status),
    developerPayload: flutterPurchase.developerPayload,
    sandbox: flutterPurchase.sandbox,
  );
}

PurchaseStatus mapFlutterPurchaseStatusToPurchaseStatus(
    FlutterPurchaseStatus? flutterPurchaseStatus) {
  return switch (flutterPurchaseStatus) {
    FlutterPurchaseStatus.invoiceCreated => PurchaseStatus.invoiceCreated,
    FlutterPurchaseStatus.cancelled => PurchaseStatus.cancelled,
    FlutterPurchaseStatus.processing => PurchaseStatus.processing,
    FlutterPurchaseStatus.rejected => PurchaseStatus.rejected,
    FlutterPurchaseStatus.confirmed => PurchaseStatus.confirmed,
    FlutterPurchaseStatus.refunded => PurchaseStatus.refunded,
    FlutterPurchaseStatus.paid => PurchaseStatus.paid,
    FlutterPurchaseStatus.reversed => PurchaseStatus.reversed,
    null => PurchaseStatus.unknown
  };
}
