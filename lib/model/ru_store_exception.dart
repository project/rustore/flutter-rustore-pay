sealed class RuStoreException implements Exception {
  const RuStoreException([this.message]);

  final String? message;
}

/// Ошибка сетевого взаимодействия SDK.
final class RustorePaymentNetworkException extends RuStoreException {
  const RustorePaymentNetworkException([super.message]);

  @override
  String toString() => '$RustorePaymentNetworkException(message: $message)';
}

/// Общая ошибка SDK.
final class RustorePaymentCommonException extends RuStoreException {
  const RustorePaymentCommonException([super.message]);

  @override
  String toString() => '$RustorePaymentCommonException(message: $message)';
}

/// Ошибка повторной инициализации SDK.
final class RuStorePayClientAlreadyExist extends RuStoreException {
  const RuStorePayClientAlreadyExist([super.message]);

  @override
  String toString() => '$RuStorePayClientAlreadyExist(message: $message)';
}

/// Попытка обратиться к публичным интерфейсам SDK до момента её инициализации.
final class RuStorePayClientNotCreated extends RuStoreException {
  const RuStorePayClientNotCreated([super.message]);

  @override
  String toString() => '$RuStorePayClientNotCreated(message: $message)';
}

/// Запущено флоу оплаты неизвестного типа продукта.
final class RuStorePayInvalidActivePurchase extends RuStoreException {
  const RuStorePayInvalidActivePurchase([super.message]);

  @override
  String toString() => '$RuStorePayInvalidActivePurchase(message: $message)';
}

/// Не задан обязательный параметр сonsole_application_id для инициализации SDK.
final class RuStorePayInvalidConsoleAppId extends RuStoreException {
  const RuStorePayInvalidConsoleAppId([super.message]);

  @override
  String toString() => '$RuStorePayInvalidConsoleAppId(message: $message)';
}

/// Неверная сигнатура ответа (возникает при попытке совершить мошеннические действия).
final class RuStorePaySignatureException extends RuStoreException {
  const RuStorePaySignatureException([super.message]);

  @override
  String toString() => '$RuStorePaySignatureException(message: $message)';
}

/// Ошибка получения платежного токена
final class EmptyPaymentTokenException extends RuStoreException {
  const EmptyPaymentTokenException([super.message]);

  @override
  String toString() => '$EmptyPaymentTokenException(message: $message)';
}

/// На устройстве пользователя не установлен RuStore.
final class RuStoreNotInstalledException extends RuStoreException {
  const RuStoreNotInstalledException([super.message]);

  @override
  String toString() => '$RuStoreNotInstalledException(message: $message)';
}

/// RuStore установленный на устройстве пользователя не поддерживает платежи.
final class RuStoreOutdatedException extends RuStoreException {
  const RuStoreOutdatedException([super.message]);

  @override
  String toString() => '$RuStoreOutdatedException(message: $message)';
}

/// Пользователь не авторизован в RuStore.
final class RuStoreUserUnauthorizedException extends RuStoreException {
  const RuStoreUserUnauthorizedException([super.message]);

  @override
  String toString() => '$RuStoreUserUnauthorizedException(message: $message)';
}

/// Приложение забанено в RuStore.
final class RuStoreApplicationBannedException extends RuStoreException {
  const RuStoreApplicationBannedException([super.message]);

  @override
  String toString() => '$RuStoreApplicationBannedException(message: $message)';
}

/// Пользователь забанен в RuStore.
final class RuStoreUserBannedException extends RuStoreException {
  const RuStoreUserBannedException([super.message]);

  @override
  String toString() => '$RuStoreUserBannedException(message: $message)';
}
