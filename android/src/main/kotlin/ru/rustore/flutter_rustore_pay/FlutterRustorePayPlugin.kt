package ru.rustore.flutter_rustore_pay

import android.content.Intent
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.PluginRegistry.NewIntentListener

/** FlutterRustorePayPlugin */
class FlutterRustorePayPlugin : FlutterPlugin, ActivityAware, NewIntentListener {

    private var client: FlutterRuStorePayClient? = null
    private var activityBinding: ActivityPluginBinding? = null

    override fun onAttachedToEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        client = FlutterRuStorePayClient().also {
            RustorePurchaseApi.setUp(binding.binaryMessenger, it)
            RustoreProductApi.setUp(binding.binaryMessenger, it)
            RustoreUtilsApi.setUp(binding.binaryMessenger, it)
        }
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {}

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activityBinding = binding
        client?.setActivityContext(binding.activity)
        binding.addOnNewIntentListener(this)
    }

    override fun onNewIntent(intent: Intent): Boolean {
        return client?.onNewIntent(intent) ?: run {
            false
        }
    }

    override fun onDetachedFromActivityForConfigChanges() {
        client?.resetActivityContext()
        activityBinding?.removeOnNewIntentListener(this)
        activityBinding = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        activityBinding = binding
        client?.setActivityContext(binding.activity)
        binding.addOnNewIntentListener(this)
    }

    override fun onDetachedFromActivity() {
        client?.resetActivityContext()
        activityBinding?.removeOnNewIntentListener(this)
        activityBinding = null
    }
}
