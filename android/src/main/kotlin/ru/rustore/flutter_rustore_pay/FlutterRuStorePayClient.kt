package ru.rustore.flutter_rustore_pay

import Cancelled
import Failure
import FlutterPreferredPurchaseType
import FlutterPurchaseAvailability
import FlutterProduct
import FlutterPurchase
import FlutterProductType
import FlutterProductPurchaseResult
import RustoreUtilsApi
import RustoreProductApi
import RustorePurchaseApi
import Success
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import ru.rustore.flutter_rustore_pay.utils.toFlutterProduct
import ru.rustore.flutter_rustore_pay.utils.toFlutterProductPurchaseResult
import ru.rustore.flutter_rustore_pay.utils.toFlutterPurchase
import ru.rustore.flutter_rustore_pay.utils.toFlutterResult
import ru.rustore.flutter_rustore_pay.utils.toPreferredPurchaseType
import ru.rustore.flutter_rustore_pay.utils.toProductType
import ru.rustore.sdk.core.util.RuStoreUtils
import ru.rustore.sdk.pay.RuStorePayClient
import ru.rustore.sdk.pay.model.AppUserId
import ru.rustore.sdk.pay.model.DeveloperPayload
import ru.rustore.sdk.pay.model.OrderId
import ru.rustore.sdk.pay.model.PreferredPurchaseType
import ru.rustore.sdk.pay.model.ProductId
import ru.rustore.sdk.pay.model.ProductPurchaseParams
import ru.rustore.sdk.pay.model.PurchaseAvailabilityResult
import ru.rustore.sdk.pay.model.PurchaseId
import ru.rustore.sdk.pay.model.Quantity
import ru.rustore.sdk.pay.model.RustorePaymentException

class FlutterRuStorePayClient : RustorePurchaseApi, RustoreProductApi, RustoreUtilsApi {

    private var context: Context? = null

    override fun getPurchase(purchaseId: String, callback: (Result<FlutterPurchase>) -> Unit) {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchase(PurchaseId(purchaseId))
            .toFlutterResult(callback) { purchase ->
                purchase.toFlutterPurchase()
            }
    }

    override fun getPurchases(
        productType: FlutterProductType?,
        callback: (Result<List<FlutterPurchase>>) -> Unit
    ) {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchases(productType?.toProductType())
            .toFlutterResult(callback) { purchases ->
                purchases.map { purchase ->
                    purchase.toFlutterPurchase()
                }
            }
    }

    override fun getPurchaseAvailability(callback: (Result<FlutterPurchaseAvailability>) -> Unit) {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchaseAvailability()
            .toFlutterResult(callback) { result ->
                when (result) {
                    PurchaseAvailabilityResult.Available -> FlutterPurchaseAvailability(true)
                    is PurchaseAvailabilityResult.Unavailable -> FlutterPurchaseAvailability(
                        false, result.cause.localizedMessage
                    )
                }
            }
    }

    override fun getProducts(ids: List<String>, callback: (Result<List<FlutterProduct>>) -> Unit) {
        RuStorePayClient.instance.getProductInteractor().getProducts(ids.map { ProductId(it) })
            .toFlutterResult(callback) { products ->
                products.map { product ->
                    product.toFlutterProduct()
                }
            }
    }

    override fun purchase(
        productId: String,
        orderId: String?,
        quantity: Long?,
        developerPayload: String?,
        appUserId: String?,
        preferredPurchaseType: FlutterPreferredPurchaseType?,
        callback: (Result<FlutterProductPurchaseResult>) -> Unit
    ) {
        RuStorePayClient.instance.getPurchaseInteractor().purchase(
            params = ProductPurchaseParams(ProductId(productId),
                quantity?.let { Quantity(it.toInt()) },
                orderId?.let { OrderId(it) },
                developerPayload?.let { DeveloperPayload(it) },
                appUserId?.let { AppUserId(it) }
            ),
            preferredPurchaseType = preferredPurchaseType.toPreferredPurchaseType(),
        ).toFlutterProductPurchaseResult(callback)
    }

    override fun purchaseTwoStep(
        productId: String,
        orderId: String?,
        quantity: Long?,
        developerPayload: String?,
        callback: (Result<FlutterProductPurchaseResult>) -> Unit
    ) {
        RuStorePayClient.instance.getPurchaseInteractor().purchaseTwoStep(
            ProductPurchaseParams(
                ProductId(productId),
                quantity?.let { Quantity(it.toInt()) },
                orderId?.let { OrderId(it) },
                developerPayload?.let { DeveloperPayload(it) })
        ).toFlutterProductPurchaseResult(callback)
    }

    override fun confirmTwoStepPurchase(
        purchaseId: String, developerPayload: String?, callback: (Result<Unit>) -> Unit
    ) {
        RuStorePayClient.instance.getPurchaseInteractor()
            .confirmTwoStepPurchase(
                PurchaseId(purchaseId),
                developerPayload?.let { DeveloperPayload(it) })
            .toFlutterResult(callback)
    }

    override fun cancelTwoStepPurchase(
        purchaseId: String, callback: (Result<Unit>) -> Unit
    ) {
        RuStorePayClient.instance.getPurchaseInteractor()
            .cancelTwoStepPurchase(PurchaseId(purchaseId))
            .toFlutterResult(callback)
    }

    override fun isRuStoreInstalled(): Boolean =
        context?.let { RuStoreUtils.isRuStoreInstalled(it) } ?: false

    override fun openRuStore() {
        context?.let { RuStoreUtils.openRuStore(it) }
    }

    override fun openRuStoreAuthorization() {
        context?.let { RuStoreUtils.openRuStoreAuthorization(it) }
    }

    override fun openRuStoreDownloadInstruction() {
        context?.let { RuStoreUtils.openRuStoreDownloadInstruction(it) }
    }

    fun onNewIntent(intent: Intent): Boolean {
        RuStorePayClient.instance.getIntentInteractor().proceedIntent(intent)
        return true
    }

    fun setActivityContext(activity: Activity) {
        context = activity
    }

    fun resetActivityContext() {
        context = null
    }
}
