package ru.rustore.flutter_rustore_pay.utils

import ru.rustore.sdk.pay.model.Product
import ru.rustore.sdk.core.tasks.Task
import ru.rustore.sdk.pay.model.Purchase
import FlutterPurchase
import FlutterProduct
import Cancelled
import Failure
import FlutterPreferredPurchaseType
import FlutterProductPurchaseResult
import Success
import FlutterProductType
import FlutterPurchaseType
import ru.rustore.sdk.pay.model.PreferredPurchaseType
import ru.rustore.sdk.pay.model.ProductPurchaseResult
import ru.rustore.sdk.pay.model.ProductType
import ru.rustore.sdk.pay.model.PurchaseType
import ru.rustore.sdk.pay.model.RustorePaymentException

fun Task<Unit>.toFlutterResult(
    callback: (Result<Unit>) -> Unit,
) {
    this
        .addOnSuccessListener { _ ->
            callback(Result.success(Unit))
        }
        .addOnFailureListener { error ->
            callback(Result.failure(error))
        }
}

fun <T, R> Task<T>.toFlutterResult(
    callback: (Result<R>) -> Unit,
    map: (T) -> R
) {
    this
        .addOnSuccessListener { value ->
            callback(runCatching {
                map(value)
            })
        }
        .addOnFailureListener { error ->
            callback(Result.failure(error))
        }
}

fun Purchase.toFlutterPurchase(): FlutterPurchase {
    return FlutterPurchase(
        purchaseId = this.purchaseId.value,
        productId = this.productId.value,
        invoiceId = this.invoiceId.value,
        orderId = this.orderId?.value,
        purchaseType = this.purchaseType.toFlutterPurchaseType(),
        productType = this.productType.toFlutterProductType(),
        description = this.description.value,
        purchaseTime = this.purchaseTime.toString(),
        price = this.price.value.toLong(),
        amountLabel = this.amountLabel.value,
        currency = this.currency.value,
        quantity = this.quantity.value.toLong(),
        status = this.status.let { FlutterPurchaseStatus.valueOf(it.toString()) },
        developerPayload = this.developerPayload?.value,
        sandbox = this.sandbox,
    )
}

fun Product.toFlutterProduct(): FlutterProduct =
    FlutterProduct(
        productId = this.productId.value,
        type = this.type.toFlutterProductType(),
        amountLabel = this.amountLabel.value,
        price = this.price?.value?.toLong(),
        currency = this.currency.value,
        imageUrl = this.imageUrl.value,
        title = this.title.value,
        description = this.description?.value
    )


fun Task<ProductPurchaseResult>.toFlutterProductPurchaseResult(
    callback: (Result<FlutterProductPurchaseResult>) -> Unit,
) {
    this
        .addOnSuccessListener { result ->
            val purchaseResult = FlutterProductPurchaseResult(
                success = Success(
                    orderId = result.orderId?.value,
                    purchaseId = result.purchaseId.value,
                    productId = result.productId.value,
                    invoiceId = result.invoiceId.value,
                    purchaseType = result.purchaseType.toFlutterPurchaseType(),
                    sandbox = result.sandbox,
                )
            )
            callback(Result.success(purchaseResult))
        }.addOnFailureListener { error ->
            when (error) {
                is RustorePaymentException.ProductPurchaseCancelled -> {
                    val result = FlutterProductPurchaseResult(
                        cancelled = Cancelled(
                            purchaseId = error.purchaseId?.value,
                            purchaseType = error.purchaseType.toFlutterPurchaseType(),
                        )
                    )
                    callback(Result.success(result))
                }

                is RustorePaymentException.ProductPurchaseException -> {
                    val result = FlutterProductPurchaseResult(
                        failure = Failure(
                            orderId = error.orderId?.value,
                            purchaseId = error.purchaseId?.value,
                            productId = error.productId?.value,
                            invoiceId = error.invoiceId?.value,
                            quantity = error.quantity?.value?.toLong(),
                            purchaseType = error.purchaseType.toFlutterPurchaseType(),
                            errorMessage = error.cause.toString(),

                            )
                    )
                    callback(Result.success(result))
                }
            }

        }
}

fun ProductType?.toFlutterProductType(): FlutterProductType =
    when (this) {
        ProductType.APPLICATION -> FlutterProductType.UNDEFINED
        ProductType.NON_CONSUMABLE_PRODUCT -> FlutterProductType.NON_CONSUMABLE_PRODUCT
        ProductType.CONSUMABLE_PRODUCT -> FlutterProductType.CONSUMABLE_PRODUCT
        null -> FlutterProductType.UNDEFINED
    }

fun PurchaseType?.toFlutterPurchaseType(): FlutterPurchaseType =
    when (this) {
        PurchaseType.ONE_STEP -> FlutterPurchaseType.ONE_STEP
        PurchaseType.TWO_STEP -> FlutterPurchaseType.TWO_STEP
        PurchaseType.UNDEFINED -> FlutterPurchaseType.UNDEFINED
        null -> FlutterPurchaseType.UNDEFINED
    }

fun FlutterProductType?.toProductType(): ProductType? =
    when (this) {
        FlutterProductType.NON_CONSUMABLE_PRODUCT -> ProductType.NON_CONSUMABLE_PRODUCT
        FlutterProductType.CONSUMABLE_PRODUCT -> ProductType.CONSUMABLE_PRODUCT
        else -> null
    }

fun FlutterPreferredPurchaseType?.toPreferredPurchaseType(): PreferredPurchaseType =
    when (this) {
        FlutterPreferredPurchaseType.ONE_STEP -> PreferredPurchaseType.ONE_STEP
        FlutterPreferredPurchaseType.TWO_STEP -> PreferredPurchaseType.TWO_STEP
        null -> PreferredPurchaseType.ONE_STEP
    }