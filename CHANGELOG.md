## 8.0.0

*  Updated RuStore Pay SDK to 8.0.0
*  `purchaseOneStep` method has been replaced by `purchase`
*  Added support for deep links

## 7.0.0

*  Initial release.
