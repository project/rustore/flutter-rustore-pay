## flutter_rustore_pay_example

### [🔗Документация разработчика](https://www.rustore.ru/help/sdk/pay/flutter)

### Содержание

- [Подготовка требуемых параметров](#подготовка-требуемых-параметров)
- [Настройка примера приложения](#настройка-примера-приложения)
- [Сценарий использования](#сценарий-использования)
- [Условия распространения](#условия-распространения)
- [Техническая поддержка](#техническая-поддержка)

## Подготовка требуемых параметров

Для корректной настройки примера приложения вам следует подготовить:

1. `consoleApplicationId` - код приложения из консоли разработчика RuStore (пример: <https://console.rustore.ru/apps/123456>), тут `consoleApplicationId` = 123456
2. `applicationId` - из приложения, которое вы публиковали в консоль RuStore, находится в файле build.gradle вашего проекта

   ```
    android {
       defaultConfig {
          applicationId = "ru.rustore.sdk.payexample"
       }
    }
   ```

3. `productIds` - [покупки](https://www.rustore.ru/help/developers/monetization/create-paid-product-in-application) доступные в вашем приложении.
4. `release.keystore` - подпись, которой было подписано приложение, опубликованное в консоль RuStore.
5. `release.properties` - в этом файле должны быть указаны параметры подписи, которой было подписано приложение, опубликованное в консоль RuStore. [Как работать с ключами подписи APK-файлов](https://www.rustore.ru/help/developers/publishing-and-verifying-apps/app-publication/apk-signature/)


## Настройка примера приложения

1. Перед вызовом методов библиотеки необходимо выполнить ее инициализацию. Сама инициализация происходит автоматически, но для работы SDK в вашем AndroidManifest.xml файле необходимо прописать console_app_id_key.
`consoleApplicationId` - код приложения из консоли разработчика RuStore (пример: https://console.rustore.ru/apps/123456), тут `consoleApplicationId` = 123456

    ```kotlin
    <meta-data
        android:name="console_app_id_key"
        android:value="@string/CONSOLE_APPLICATION_ID" />
    ```

    В strings.xml заменить строку с вашим app_id
    ```kotlin
    <string name="CONSOLE_APPLICATION_ID">your_app_id</string>
    ```

2. Замените `applicationId`, в файле example/android/app/build.gradle, на applicationId apk-файла, который вы публиковали в консоль RuStore:

   ```
   android {
       defaultConfig {
          applicationId = "ru.rustore.sdk.flutterpayexample" // Зачастую в buildTypes приписывается .debug
       }
   }
   ```
3. В директорию example/android положите файлы `release.keystore` и `release.properties`.
4. В файле `release.properties` выполните настройку параметров `key_alias`, `key_password`, `store_password`. Подпись `release.keystore` должна совпадать с подписью, которой было подписано приложение, опубликованное в консоль RuStore. Убедитесь, что используемый `buildType` (пр. debug) использует такую же подпись, что и опубликованное приложение (пр. release).
5. В `ProductWidget` в переменной `ids` продукты доступные в вашем приложении:

   ```
   final List<String> ids = [
        "product1",
        "product2",
        "product3",
    ];
   ```

5. Запустите проект и проверьте работу приложения


## Сценарий использования

### Инициализация

Перед вызовом методов библиотеки необходимо выполнить ее инициализацию. Сама инициализация происходит автоматически, но для работы SDK в вашем AndroidManifest.xml файле необходимо прописать console_app_id_value.
`console_app_id_value` - код приложения из консоли разработчика RuStore (пример: https://console.rustore.ru/apps/123456), тут `console_app_id_value` = 123456

```kotlin
<meta-data
    android:name="console_app_id_value"
    android:value="@string/CONSOLE_APPLICATION_ID" />
```

В strings.xml добавить строку с вашим app_id
```kotlin
<string name="CONSOLE_APPLICATION_ID">your_app_id</string>
```

### Обработка deeplink

Для корректной работы оплаты через сторонние приложения (СБП или SberPay), требуется правильно реализовать обработку deeplink. 
Для этого необходимо:
1) Указать в AndroidManifest.xml intent-filter с scheme вашего проекта c ссылкой на значение из ресурсов.
2) Добавить метадату с именем `sdk_pay_scheme_value` c ссылкой на значение из ресурсов.

```
<activity
android:name=".sample.MainActivity">  
    <intent-filter>
        <action android:name="android.intent.action.VIEW" />
        <category android:name="android.intent.category.DEFAULT" />
        <category android:name="android.intent.category.BROWSABLE" />
        <data android:scheme="@string/SDK_PAY_SCHEME" /> />
    </intent-filter>
    
    <meta-data
       android:name="sdk_pay_scheme_value"
       android:value="@string/SDK_PAY_SCHEME" />

</activity>
```

В strings.xml добавить строку с вашим sdk_pay_scheme
```kotlin
<string name="SDK_PAY_SCHEME">you_deep_link_scheme</string>
```

### Работа с покупками(`RuStoreFlutterPurchaseInteractor` - позволяет работать с платежами и имеет несколько публичных методов)

#### Получение списка покупок

Для получения списка покупок необходимо использовать метод `getPurchases({ProductType? productType})`

Данный метод поддерживает опциональную фильтрацию по типу товаров (потребляемые или непотреблямые товары).
По умолчанию фильтр выключен и вернет всё покупки пользователя (в не зависимости от типа товара) в статусах paid и confirmed.

Статус paid означает успешное холдирование средств, покупка ожидает подтверждения покупки со стороны разработчика.

```
RuStorePayClient.instance.purchaseInteractor.getPurchases().then((purchases) {
  
});
```

Метод возвращает `List<Purchase>`:

#### Получение информации о покупке

```
RuStorePayClient.instance.purchaseInteractor.getPurchase(purchaseId).then((purchase) {

});
```
- purchaseId: String - идентификатор продукта.

Метод возвращает `Purchase`:


Структура покупки `Purchase`:
```
class Purchase {
   String purchaseId;
   String productId;
   String invoiceId;
   String? orderId;
   PurchaseType purchaseType;
   ProductType productType;
   String description;
   String? purchaseTime;
   int price;
   String amountLabel;
   String currency;
   int quantity;
   PurchaseStatus status;
   String? developerPayload;
   bool sandbox;
}
```
- purchaseId - идентификатор покупки.
- productId - идентификатор продукта, указанный при создании в консоли разработчика RuStore.
- invoiceId - идентификатор счета.
- orderId - уникальный идентификатор оплаты, либо переданный разработчиком, либо сформированный автоматически (uuid)
- productType - тип продукта (потребляемый / непотребляемый).
- purchaseType - тип покупки (одностадийная / двустадийная).
- description - описание покупки.
- purchaseTime - время покупки.
- price - цена в минимальных единицах валюты.
- amountLable - отформатированная цена покупки, включая валютный знак.
- currency - код валюты ISO 4217.
- quantity - количество продукта.
- status - состояние покупки.
- Возможные значения состояния покупки:
   - invoice_created - создана, ожидает оплаты.
   - cancelled - покупка отменена.
   - processing - запущена оплата.
   - rejected - покупка отклонена.
   - confirmed -платеж за непотребляемый товар успешно совершен.
   - refunded - совершен возврат средств за покупку.
   - paid - для потребляемых товаров - холдирование средств на карте пользователя прошло успешно, покупка ожидает подтверждения.
   - reversed - покупка была отменена, т.к. не было произведено подтверждение в течение 72 часов (только для потребляемых товаров).
- developerPayload - указанная разработчиком строка, содержащая дополнительную информацию о заказе
- sandbox - флаг, указывающий признак тестового платежа в песочнице. Если true - покупка совершена в режиме тестирования

Структура `PurchaseType`:

```
enum PurchaseType {
  oneStep,
  twoStep,
  undefined
}
```

Структура `ProductType`:

```
enum ProductType {
  consumable,
  nonConsumable,
  undefined
}
```

#### Проверка доступности работы с платежами

Для проверки доступности платежей необходимы следующие условия:

- На устройстве пользователя должен быть установлен RuStore.
- Пользователь должен быть авторизован в RuStore.
- Пользователь и приложение не должны быть заблокированы в RuStore.
- У компании подключена монетизация через консоль разработчика RuStore

```
RuStorePayClient.instance.purchaseInteractor.getPurchaseAvailability().then((value) {
  if (value case Available _) {
    // Process purchases available
  } else if (value case Unavailable unavailable) {
    // Process purchases unavailable
  }
});
```

#### Покупка продукта (Оплата с выбором типа покупки)

Для вызова покупки продукта с выбором стадийности оплаты используйте метод purchase(id):

```
RuStorePayClient.instance.purchaseInteractor
    .purchase(id).then((value) {
    
});
```

- У метода есть не опциональные и опциональные параметры:
productId - идентификатор продукта, который требуется приобрести (обязательный параметр).
quantity - количество продуктов - опционально. Если не указывать, то будет подставлено значение 1. Не применимо для покупки непотребляемого товара.
orderId  - идентификатор заказа, создаётся на стороне приложения - опционально. Если не указан, то генерируется автоматически. Максимальная длина 150 символов
developerPayload - дополнительная информация от разработчика приложения (опционально). Максимальная длина 250 символов
appUserId - внутренний ID пользователя в приложении - опционально. Максимальная длина 128 символов
preferredPurchaseType - желаемый тип покупки - одностадийная (oneStep) или двухстадийная (twoStep) (опциональный параметр, по умолчанию oneStep).

Структура `PreferredPurchaseType`:

```
enum PreferredPurchaseType {
  oneStep,
  twoStep
}
```

Метод возвращает `ProductPurchaseResult`:

Данный метод по умолчанию запускается по одностадийному сценарию оплаты (preferredPurchaseType = PreferredPurchaseType.oneStep), т.е. без холдирования средств.
Для двустадийной оплаты нужно указать preferredPurchaseType = PreferredPurchaseType.TWO_STEP. Двустадийная оплата (т.е. оплата с холдированием средств) для данного метода - негарантированная, и напрямую будет зависеть от того, какой способом оплаты (карта, СПБ и др.) будет выбран пользователем.

#### Покупка продукта (Двустадийная оплата (с холдированием средств))

При вызове данного метода пользователю будет доступен ограниченный набор способов оплаты - только те, которые поддерживают двухстадийную оплату

Для вызова покупки продукта используйте метод `purchaseTwoStep(id)`:

```
RuStorePayClient.instance.purchaseInteractor.purchaseTwoStep(id).then((value) {

});
```

- У метода есть не опциональные и опциональные параметры:
productId - идентификатор продукта (обязательный параметр).
quantity - количество продуктов (опционально. Если не указывать, то будет подставлено значение 1).
orderId  - идентификатор заказа, создаётся на стороне приложения. (опционально. Если не указан, то генерируется автоматически). Максимальная длина 150 символов
developerPayload - дополнительная информация от разработчика приложения (опционально). Максимальная длина 250 символов
appUserId - внутренний ID пользователя в приложении - опционально. Максимальная длина 128 символов

Метод возвращает `ProductPurchaseResult`:

Структура результата покупки ProductPurchaseResult:

```
class ProductPurchaseResult {
   Success? success;
   Cancelled? cancelled;
   Failure? failure;
}
```

Структура `Success`:

```
class Success {
   String? orderId;
   String purchaseId;
   String productId;
   String invoiceId;
   PurchaseType purchaseType;
   bool sandbox;
}
```

Структура `Cancelled`:

```
class Cancelled {
   String? purchaseId;
   PurchaseType purchaseType;
}
```

Структура `Failure`:

```
class Failure {
   String? orderId;
   String? purchaseId;
   String? productId;
   String? invoiceId;
   int? quantity;
   String errorMessage;
   PurchaseType purchaseType
}
```

- `SuccessProductPurchaseResult` - результат успешного завершения покупки цифрового товара.
- `FailureProductPurchaseResult` - результат ошибки покупки цифрового товара.
- `CancelProductPurchaseResult` - платёжный диалог закрыт до получения результата покупки. Состояние покупки неизвестно. Рекомендуем запросить статус покупки отдельно методом получения информации о покупке.

purchaseId - идентификатор покупки. Используется для получения информации о покупке в SDK методом получения информации о покупке
productId - идентификатор приобретенного продукта, указанный при создании в консоли разработчика RuStore.
invoiceId - идентификатор счета. Используется для серверной валидации платежа, поиска платежей в консоли разработчика, а также отображается покупателю в истории платежей в мобильном приложении RuStore
orderId - уникальный идентификатор оплаты, указанный разработчиком или сформированный автоматически (uuid).
purchaseType - тип покупки (ONE_STEP/TWO_STEP - одностадийная\двухстадийная)
sandbox - флаг, указывающий признак тестового платежа в песочнице. Если TRUE - покупка совершена в режиме тестирования
errorMessage - сообщение с причиной ошибки

#### Пояснения по работе с одностадийными и двухстадийными оплатами

При использовании одностадийного платежа покупка не требует подтверждения, денежные средства сразу списываются со счета покупателя, а с разработчика удерживается комиссия. В таком случае, если требуется вернуть денежные средства клиенту (например, по какой-то причине нет возможности выдать продукт), возможен только возврат средств через консоль разработчика, денежные средства возвращаются покупателю через несколько дней. Возвращается полная стоимость покупки, при этом удержанная комиссия разработчику не возмещается.

В случае использования двухстадийного платежа сначала производится холдирование средств на счете покупателя. Комиссия в этом случае не удерживается. После холдирования покупка требует подтверждения или отмены. Комиссия с разработчика удерживается при подтверждении покупки. Отмена покупки означает снятие холда - денежные средства мгновенно снова доступны покупателю. ВАЖНО! Не все способы оплаты поддерживают двухстадийную оплату.

#### Подтверждение покупки
Подтверждения требуют только покупки, которые были запущены по двустадийному флоу оплаты, т.е. с холдированием средств.
Такие покупки, после успешной оплаты будут находиться в статусе PurchaseStatus.paid.
Для списания средств с карты покупателя требуется подтверждение покупки. Для этого вы должны использовать метод confirmTwoStepPurchase:

```
RuStorePayClient.instance.purchaseInteractor.confirmTwoStepPurchase(id).then((value) {

});
```

- У метода есть не опциональные и опциональные параметры:
purchaseId - идентификатор покупки.
developerPayload - дополнительная информация от разработчика AnyApp (опционально). Максимально 250 символов. Если передан, заменяет значение, записанное при старте покупки методом purchase/purchaseTwoStep.

#### Отмена покупки
Через SDK можно отменять только те покупки, которые были запущены по двухстадийному флоу оплаты, т.е. с холдированием средств. Такие покупки, после успешного холдирования будут находиться в статусе PurchaseStatus.paid. После отмены покупки будут переходить в статус PurchaseStatus.reversed.

Используйте отмену покупки в случаях, если после оплаты (холдирования средств) вы не можете предоставить покупателю товар.

Для отмены покупки (холда) вы должны использовать метод cancelTwoStepPurchase:

```
RuStorePayClient.instance.purchaseInteractor.cancelTwoStepPurchase(id).then((value) {

});
```

- id - идентификатор покупки.

### Работа с продуктами(`RuStoreFlutterProductsInteractor` - позволяет работать с продуктами и имеет несколько публичных методов)

#### Получение списка продуктов

Для получения продуктов необходимо использовать метод `getProducts(ids)`:

```
RuStorePayClient.instance.productInteractor.getProducts(ids).then((products) {

});
```
`ids: List<String>` - список идентификаторов продуктов.

Метод возвращает `List<Product>`:

Структура продукта `Product`:

```
class Product {
   String productId;
   ProductType type;
   String amountLabel;
   int? price;
   String currency;
   String imageUrl;
   String title;
   String? description;
}
```

- productId - идентификатор продукта, указанный при создании продукта в консоли разработчика.
- type - тип продукта (потребляемый / непотребляемый).
- amountLabel - отформатированная цена товара, включая валютный знак.
- price - цена в минимальных единицах (копейках).
- currency - код валюты ISO 4217.
- title - название продукта.
- description - описание продукта.
- imageUrl - ссылка на иконку продукта.

Структура `ProductType`:

```
enum ProductType {
  consumable,
  nonConsumable,
  undefined
}
```

### Утилитные методы(`RuStoreFlutterUtils` - набор публичных методов)

#### Проверка наличия приложения RuStore на девайсе пользователя

```
RuStorePayClient.instance.ruStoreUtils.isRuStoreInstalled().then((isInstalled) {

});
```

Метод возвращает логическое значение `isInstalled`

#### Открытие web страницы для скачивания МП RuStore

```
RuStorePayClient.instance.ruStoreUtils.openRuStoreDownloadInstruction()
```
#### Запуск МП RuStore

```
RuStorePayClient.instance.ruStoreUtils.openRuStore()
```
#### Запуск МП RuStore для авторизации. После успешной авторизации пользователя МП RuStore автоматически закроется

```
RuStorePayClient.instance.ruStoreUtils.openRuStoreAuthorization()
```


## Условия распространения

Данное программное обеспечение, включая исходные коды, бинарные библиотеки и другие файлы распространяется под лицензией MIT. Информация о лицензировании доступна в документе `LICENSE.txt`

## Техническая поддержка

Если появились вопросы по интеграции SDK платежей, обратитесь по [ссылке](https://www.rustore.ru/help/sdk/pay/flutter).
