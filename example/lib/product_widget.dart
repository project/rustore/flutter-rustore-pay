import 'package:flutter/material.dart';
import 'package:flutter_rustore_pay/api/flutter_rustore_pay_client.dart';
import 'package:flutter_rustore_pay/model/preferred_purchase_type.dart';
import 'package:flutter_rustore_pay/model/product.dart';
import 'package:flutter_rustore_pay/model/purchase_result.dart';

final List<String> ids = ["product_1", "product_2", "product_3"];

class ProductWidget extends StatefulWidget {
  const ProductWidget({super.key});

  @override
  State<ProductWidget> createState() => _ProductWidget();
}

class _ProductWidget extends State<ProductWidget> {
  List<Product?> products = [];

  void _showToast(BuildContext context, String text) async {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(text),
      ));
  }

  void _purchase(String id) {
    RuStorePayClient.instance.purchaseInteractor
        .purchase(id, preferredPurchaseType: PreferredPurchaseType.oneStep)
        .then((value) {
          if (value case SuccessProductPurchaseResult _) {
            _showToast(context, "Одностадийная покупка прошла успешно");
          }
    });
  }

  void _purchaseTwoStep(String id) {
    RuStorePayClient.instance.purchaseInteractor
        .purchaseTwoStep(id)
        .then((value) {
      if (value case SuccessProductPurchaseResult _) {
        _showToast(context, "Двустадийная покупка прошла успешно, теперь необходимо подтвердить ее, для этого перейди в покупки");
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _reloadProducts();
  }

  void _reloadProducts() {
    RuStorePayClient.instance.productInteractor
        .getProducts(ids)
        .then((products) {
      setState(() {
        this.products = products;
      });
    });
  }

  Widget _buildText(String text,
      {double fontSize = 16,
      FontWeight fontWeight = FontWeight.normal,
      Color? color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color ?? Colors.black,
        ),
      ),
    );
  }

  void _showDialogWithButtons(BuildContext context, String productId) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Выбери метод покупки'),
          actions: <Widget>[
            TextButton(
              child: const Text('purchase'),
              onPressed: () {
                _purchase(productId);
                Navigator.of(context).pop(); // Закрыть диалог
              },
            ),
            TextButton(
              child: const Text('purchaseTwoStep'),
              onPressed: () {
                _purchaseTwoStep(productId);
                Navigator.of(context).pop(); // Закрыть диалог
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildItem(Product product) {
    return InkWell(
        onTap: () {
          _showDialogWithButtons(context, product.productId);
        },
        child: Card(
          margin: const EdgeInsets.all(8.0),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildText('Name: ${product.title}'),
                _buildText('Desc: ${product.description ?? ""}'),
                _buildText('Type: ${product.type.name}'),
                _buildText('AmountLabel: ${product.amountLabel}'),
              ],
            ),
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          for (var product in products) ...[
            if (product != null) _buildItem(product)
          ],
        ],
      ),
    );
  }
}
