import 'package:flutter/material.dart';
import 'package:flutter_rustore_pay/api/flutter_rustore_pay_client.dart';
import 'package:flutter_rustore_pay/model/purchase.dart';

class PurchaseWidget extends StatefulWidget {
  const PurchaseWidget({super.key});

  @override
  State<PurchaseWidget> createState() => _PurchaseWidget();
}

class _PurchaseWidget extends State<PurchaseWidget> {
  List<Purchase?> purchases = [];
  bool isConsume = false;

  @override
  void initState() {
    super.initState();
    _reloadPurchases();
  }

  void _reloadPurchases() {
    RuStorePayClient.instance.purchaseInteractor
        .getPurchases()
        .then((purchases) {
      setState(() {
        this.purchases = purchases;
      });
    });
  }

  void _confirmTwoStepPurchase(String id) {
    RuStorePayClient.instance.purchaseInteractor
        .confirmTwoStepPurchase(id)
        .then((value) {
      setState(() {
        isConsume = true;
      });
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Покупка успешно подтверждена"),
      ));
    });
  }

  void _cancelTwoStepPurchase(String id) {
    RuStorePayClient.instance.purchaseInteractor
        .cancelTwoStepPurchase(id)
        .then((value) {
      setState(() {
        isConsume = true;
      });
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Покупка успешно отменена"),
      ));
    });
  }

  void _purchaseInfo(String id) {
    RuStorePayClient.instance.purchaseInteractor.getPurchase(id).then((value) {
      setState(() {});
      print('purchaseInfo: $value');
    });
  }

  Widget _buildText(String text,
      {double fontSize = 16,
      FontWeight fontWeight = FontWeight.normal,
      Color? color}) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: Text(
        text,
        style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight,
          color: color ?? Colors.black,
        ),
      ),
    );
  }

  Widget _buildItem(Purchase purchase) {
    return Card(
      margin: const EdgeInsets.all(8.0),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildText('ProductId: ${purchase.productId}'),
            _buildText('Status: ${purchase.status}'),
            _buildText('PurchaseType: ${purchase.purchaseType.name}'),
            _buildText('ProductType: ${purchase.productType.name}'),
            _buildText('PurchaseId: ${purchase.purchaseId}'),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  OutlinedButton(
                      onPressed: () {
                        _purchaseInfo(purchase.purchaseId);
                      },
                      child: const Text('Purchase Info')),
                  if ((purchase.status) == PurchaseStatus.paid && !isConsume)
                  OutlinedButton(
                    onPressed: () {
                      _confirmTwoStepPurchase(purchase.purchaseId);
                    },
                    child: const Text('Confirm'),
                  ),
                  if ((purchase.status) == PurchaseStatus.paid && !isConsume)
                  OutlinedButton(
                    onPressed: () {
                      _cancelTwoStepPurchase(purchase.purchaseId);
                    },
                    child: const Text('Cancel'),
                  ),
                ],
              ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          for (var purchase in purchases) ...[
            if (purchase != null) _buildItem(purchase)
          ],
        ],
      ),
    );
  }
}
