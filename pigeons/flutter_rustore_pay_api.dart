import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(PigeonOptions(
    dartOut: 'lib/pigeons/flutter_rustore_pay_api.dart',
    dartOptions: DartOptions(),
    kotlinOut: 'android/src/main/kotlin/pigeons/RuStorePay.kt',
    kotlinOptions: KotlinOptions()))
class FlutterProduct {
  late String productId;
  late FlutterProductType type;
  late String amountLabel;
  late int? price;
  late String currency;
  late String imageUrl;
  late String title;
  late String? description;
}

enum FlutterProductType {
  consumableProduct,
  nonConsumableProduct,
  undefined,
}

class FlutterPurchase {
  late String purchaseId;
  late String productId;
  late String invoiceId;
  late String? orderId;
  late FlutterPurchaseType purchaseType;
  late FlutterProductType productType;
  late String description;
  late String? purchaseTime;
  late int price;
  late String amountLabel;
  late String currency;
  late int quantity;
  late FlutterPurchaseStatus status;
  late String? subscriptionToken;
  late String? developerPayload;
  late bool sandbox;
}

enum FlutterPurchaseStatus {
  invoiceCreated,
  cancelled,
  processing,
  rejected,
  confirmed,
  refunded,
  paid,
  reversed;
}

enum FlutterPurchaseType {
  oneStep,
  twoStep,
  undefined,
}

enum FlutterPreferredPurchaseType {
  oneStep,
  twoStep,
}

class FlutterPurchaseAvailability {
  bool availability;
  String? cause;

  FlutterPurchaseAvailability({required this.availability, this.cause});
}

class FlutterProductPurchaseResult {
  Success? success;
  Cancelled? cancelled;
  Failure? failure;
}

class Success {
  late String? orderId;
  late String purchaseId;
  late String productId;
  late String invoiceId;
  late FlutterPurchaseType purchaseType;
  late bool sandbox;
}

class Cancelled {
  late String? purchaseId;
  late FlutterPurchaseType purchaseType;
}

class Failure {
  late String? orderId;
  late String? purchaseId;
  late String? productId;
  late String? invoiceId;
  late int? quantity;
  late FlutterPurchaseType purchaseType;
  late String errorMessage;
}

@HostApi()
abstract class RustorePurchaseApi {
  @async
  FlutterPurchase getPurchase(String purchaseId);

  @async
  List<FlutterPurchase> getPurchases({FlutterProductType? productType});

  @async
  FlutterPurchaseAvailability getPurchaseAvailability();

  @async
  FlutterProductPurchaseResult purchase(
    String productId, {
    String? orderId,
    int? quantity,
    String? developerPayload,
    String? appUserId,
    FlutterPreferredPurchaseType? preferredPurchaseType,
  });

  @async
  FlutterProductPurchaseResult purchaseTwoStep(
    String productId, {
    String? orderId,
    int? quantity,
    String? developerPayload,
  });

  @async
  void confirmTwoStepPurchase(
    String purchaseId, {
    String? developerPayload,
  });

  @async
  void cancelTwoStepPurchase(String purchaseId);
}

@HostApi()
abstract class RustoreProductApi {
  @async
  List<FlutterProduct> getProducts(List<String> ids);
}

@HostApi()
abstract class RustoreUtilsApi {
  void openRuStore();

  bool isRuStoreInstalled();

  void openRuStoreAuthorization();

  void openRuStoreDownloadInstruction();
}
